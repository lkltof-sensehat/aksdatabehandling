from sense_hat import SenseHat
import pathlib
import logging
from time import sleep

"""
SCETCH STAGE ONLY!
NOT TESTED ON A REAL SENSE-HAT

This file logs acceleration data from the raspberry pi sense-hat addon shield to a file.
"""

sense = SenseHat()

file_path = pathlib.Path("acceleration_raw_data.txt")


try:
    with file_path.open(mode="w") as file:
        file.write("Akselerasjonsdata. Enhet m/s^2\n")
        file.write("x,    y,     z,    Tid\n")
except OSError as error:
    logging.error("Skriving til fil %s gikk galt. Feilmedling:" % (file_path, error))


dt = 0.1 # tidssteg / mellomrom mellom hver måling
T = 0    # tid

while True:
    acceleration = sense.get_accelerometer_raw()

    # Datastrukturen til acceleration er en dictionary. Henter ut verdien for x, y, og z
    x = acceleration['x']
    y = acceleration['y']
    z = acceleration['z']

    try:
        with file_path.open(mode="w") as file:
            file.write(f"{x:.3f},     {y:.3f}      {z:.3f},     {T:.3f}\n")
    except OSError as error:
        logging.error("Skriving til fil %s gikk galt. Feilmedling:" % (file_path, error))
    
    T = T + dt
    sleep(dt)

        






